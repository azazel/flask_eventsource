let
  pkgs = import <nixpkgs> {};
in
  pkgs.mkShell {
    buildInputs = with pkgs;[
      gnumake
      (python37.withPackages (py: [py.flask]))
      wget
    ];
  }
