from queue import Queue
from flask import Flask, render_template, request, Response

app = Flask(__name__, template_folder='.')
app.debug = True

QUEUES = []


def event_stream(q):
    while True:
        # Question: how to break out of this loop when the app is killed?
        message = q.get(True)
        print("Sending {}".format(message))
        yield "data: {}\n\n".format(message)


@app.route('/')
def hello():
    return render_template('./index.html')


@app.route('/api/stream')
def stream():
    q = Queue()
    QUEUES.append(q)

    return Response(event_stream(q), mimetype="text/event-stream")


@app.route('/api/post', methods=['GET'])
def api_parse_sentence():
    for q in QUEUES:
        q.put(request.args.get('sentence'))

    return "OK"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True, threaded=True)
